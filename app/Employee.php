<?php

namespace hrms;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Employee extends Model implements SluggableInterface
{
	use SluggableTrait;
	/**
	 * Database tables
	 * @var string
	 */
	protected $table = 'employees';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = ['id', 'created_at', 'updated_at'];

    protected $sluggable = [
    	'build_from'	=>	'first_name',
    	'save_to'		=>	'slug'
    ];

    /**
    * Need to test this method
    * @return Array [description]
    */
    public static function interviewers()
    {
        $e =  self::all('id', 'first_name', 'last_name');
        $new_data = [];
        foreach ($e as $value) {
            $new_data[$value['id']] = $value['first_name'] . ' ' . $value['last_name'];
        }
        return $new_data;
    }

    public function interviewer()
    {
    	return $this->hasMany('hrms\Interviewer');
    }

    public function getinterviewerListAttribute()
    {
        return $this->interviewer->lists('interviewer_id')->toArray();
    }


    public function department()
    {
    	return $this->belongsTo('hrms\Department');
    }

    public function designation()
    {
    	return $this->belongsTo('hrms\Designation');
    }

}
