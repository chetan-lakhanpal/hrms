<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function(){ return view('login'); });
Route::get('Calendar', ['as' => 'Calendar.index', 'uses' => 'CalendarController@index']);

Route::resource('Employee', 'EmployeeController');
Route::resource('departments', 'DepartmentController');
Route::resource('designations', 'DesignationController');
Route::resource('sources', 'SourceController');
Route::resource('checklists', 'ChecklistController');
Route::resource('users', 'UserController');