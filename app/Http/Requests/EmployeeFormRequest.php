<?php

namespace hrms\Http\Requests;

use hrms\Http\Requests\Request;

class EmployeeFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $phone_number = 'digits_between:7,13|unique:employees';
        $personal_email = 'required|email|unique:employees';
        $emp_id = 'alpha_num|min:4|max:10|unique:employees';
        $office_email = 'email|unique:employees';

        if(Request::method() == 'PUT')
        {//We are getting this Id from Update controller.
            $id = basename($_SERVER['REQUEST_URI']);
            $phone_number .= ',phone_number,'.$id;
            $personal_email .= ',personal_email,'.$id;
            $emp_id .= ',emp_id,'.$id;
            $office_email .= ',office_email,'.$id;
        }

        return [
            'first_name' => 'required|min:4|string',
            'last_name' =>  'required|min:4|string',
            'affiliation'   =>  'required|min:3|max:4',
            'status'    =>  'required|max:1',
            'phone_number'  =>  $phone_number,
            'personal_email'    => $personal_email,
            'postal_address'    =>  'string|min:5',
            'landmark'  =>  'string|min:5',
            'emergency_no'  => 'digits_between:7,13',
            'emp_type'  =>  'string|max:10',
            'payment_type'  =>  'string|max:1',
            'emp_id'    =>  $emp_id,
            'office_email'  => $office_email,
            'dlc_name'  =>  'string|min:4',
            'prev_empcontact'   =>  'digits_between:7,13',
            'prev_empemail' =>  'email',
            'prev_empaddress'   =>  'string|min:5'
        ];
    }

}
