<?php

namespace hrms\Http\Controllers;

use Illuminate\Http\Request;

use hrms\Http\Requests;
use hrms\Http\Controllers\Controller;
use hrms\Designation;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $formname = 'Designation';
        $route = 'designations.store';
        $type = 'Name';
        $records = Designation::all();
        return view('Org.org_index_create', compact('formname', 'route', 'type', 'records'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|unique:designations|max:40|min:3']);
        Designation::create(['name' => $request->get('name')]);
        return \Redirect::route('designations.index')->with('status', 'Designation created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
       $mod = Designation::findOrFail($id);
       $formname = 'Designation';
       $type = 'Name';
       $route = "designations.update";
       return view('Org.org_index_create', compact('mod', 'formname', 'route', 'type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required|max:40|min:3|unique:designations,name,'.$id]);
        $de = Designation::find($id);
        $de->name = $request->get('name');
        $de->save();

        return \Redirect::route('designations.index')->with('status', 'Designation updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $dep = Designation::findOrFail($id);
        $name = $dep->name;
        $status = $dep->delete();

        return response()->json(['status' => $status, 'name' => $name]);
    }
}
