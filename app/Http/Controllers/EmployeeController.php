<?php

namespace hrms\Http\Controllers;

use Illuminate\Http\Request;

use hrms\Http\Requests;
use hrms\Http\Controllers\Controller;
use hrms\Http\Requests\EmployeeFormRequest;
use hrms\Employee;
use hrms\Interviewer;
use hrms\Designation;
use hrms\Department;


class EmployeeController extends Controller
{
    /**
     * Cities, States, Department, Designations
     * @return Array
     */
    private function otherInfo()
    {
        $states = ['Andra Pradesh','Arunachal Pradesh','Assam','Bihar','Chhattisgarh','Goa','Gujarat','Haryana','Himachal Pradesh','Jammu and Kashmir','Jharkhand','Karnataka','Kerala','Madya Pradesh','Maharashtra','Manipur','Meghalaya','Mizoram','Nagaland','Orissa','Punjab','Rajasthan','Sikkim','Tamil Nadu','Tripura','Uttar Pradesh','Uttaranchal','West Bengal'];
        $blood_group = ['A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-'];
        $cities = ['Agartala', 'Agra', 'Ahmedabad', 'Aizwal', 'Aligarh', 'Allahabad', 'Amritsar', 'Aurangabad', 'Bangalore', 'Baroda', 'Bhopal', 'Bhubaneshwar', 'Bokaro', 'Chandigarh', 'Chennai', 'Cochin', 'Coimbatore', 'Cuttack', 'Dehradun', 'Delhi', 'Dimapur', 'Durgapur', 'Faridabad', 'Gandhinagar', 'Ghaziabad', 'Goa', 'Gurgaon', 'Guwahati', 'Gwalior', 'Hyderabad', 'Imphal', 'Indore', 'Jabalpur', 'Jaipur', 'Jammu', 'Jamnagar', 'Jamshedpur', 'Jodhpur', 'Jullundhar', 'Kanpur', 'Kolhapur', 'Kolkata', 'Lucknow', 'Ludhiana', 'Madurai', 'Mangalore', 'Margaon', 'Mumbai', 'Mysore', 'Nagpur', 'Nasik', 'NOIDA', 'Patna', 'Panjim', 'Pondicherry', 'Pune', 'Raipur', 'Ranchi', 'Rourkela', 'Rajkot', 'Salem', 'Secunderabad', 'Shillong', 'Siliguri', 'Srinagar', 'Surat', 'Thane', 'Thiruvananthapuram', 'Tiruchirapalli', 'Udaipur', 'Varanasi', 'Vellore', 'Vijaywada', 'Vishakapatnam', 'Other'];
        $department = Department::lists('name', 'id');
        $designation = Designation::lists('name', 'id');
        $interviewer = Employee::interviewers();
        $document = [];

        return get_defined_vars();
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //Run ORM here
        $employees = Employee::all(['slug', 'affiliation', 'first_name', 'last_name', 'office_email', 'designation', 'department', 'phone_number']);
        return view('Employee.View', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('Employee.create', $this->otherInfo());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(EmployeeFormRequest $request)
    {
        //fill added_by column with Logged in user
       $data = $request->all();
       unset($data['interviewerlist']);

       $employee = Employee::create($data);
       if(array_key_exists('interviewerlist', $request->all()))
       {
            foreach ($request->input('interviewerlist') as $value):
                $interviewer = new Interviewer;
                $interviewer->interviewer_id = $value;
                $interviewer->employee_id =  $employee->id;
                $employee->interviewer()->save($interviewer);
            endforeach;
        }

        return redirect()->route('Employee.create')->with('status', 'Record created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  String  $slug
     * @return Response
     */
    public function show($slug)
    {
        $emp = Employee::findBySlug($slug);
        return view('Employee.show', compact('emp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  String  $slug
     * @return Response
     */
    public function edit($slug)
    {
        $data = $this->otherInfo();
        $employee = Employee::findBySlug($slug);
        //Removing the current Emp from Interviewer's list
        unset($data['interviewer'][$employee->id]);
        return view('Employee.update', $data, compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($id, EmployeeFormRequest $request)
    {
        $employee = Employee::findOrFail($id);

        $data = $request->all();
        unset($data['interviewerlist']);

        $stat = $employee->update($data);

        $int = \DB::table('interviewers')->where('employee_id', $id)->delete();

       if(array_key_exists('interviewerlist', $request->all()))
       {
            foreach ($request->input('interviewerlist') as $value):
                $interviewer = new Interviewer;
                $interviewer->interviewer_id = $value;
                $interviewer->employee_id =  $employee->id;
                $employee->interviewer()->save($interviewer);
            endforeach;
        }

        if($stat)
            return redirect()->route('Employee.index')->with('status', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  String  $slug
     * @return Response
     */
    public function destroy($slug)
    {
        $emp = Employee::findBySlug($slug);
        $name = $emp->first_name;
        $status = $emp->delete();

        return response()->json(['status' => $status, 'name' => $name]);
    }
}
