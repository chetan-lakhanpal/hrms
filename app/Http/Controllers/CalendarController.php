<?php

namespace hrms\Http\Controllers;

use Illuminate\Http\Request;

use hrms\Http\Requests;
use hrms\Http\Controllers\Controller;

class CalendarController extends Controller
{
    public function index()
    {
    	return view('Org.Calendar');
    }
}