<?php

namespace hrms\Http\Controllers;
//fixed
use DB;
use Illuminate\Http\Request;
use hrms\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // $users = User::find::all(['id', ]);
        $emp_list = DB::table('employees')->lists('first_name', 'id');
        $users = DB::table('employees')
                ->join('users', 'users.employee_id', '=', 'employees.id')
                ->select(['users.id', 'users.role', 'users.status', DB::raw("CONCAT(first_name, ' ', last_name) AS fullname")])
                ->get();
        $role = range(1,3);
        return view('users.create', compact('emp_list', 'role', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     * As we aren't showing form on a totally different page we are throwing 404
     * @return Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // $this->validate($request->rules[]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $mod = User::findOrFail($id);
        return view('users.create', compact('mod'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
