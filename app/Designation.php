<?php

namespace hrms;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $fillable = ['name'];

    protected $hidden = ['created_at', 'updated_at'];

    public function employee()
    {
    	return $this->hasMany('hrms\Employee', 'designation');
    }
}
