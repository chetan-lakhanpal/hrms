<?php

/*
	Add custom function that will be available in every file
 */


//Create dynamic li's
function create_li($label, $hyperlink)
{
	$active = (Route::currentRouteName() == $hyperlink)? "class='active'"	:	"";
	$a = "<li><a href='" .  URL::Route($hyperlink) . "' {$active}>" .  $label . "</a></li>";
	return $a;
}

//Create Submenu active state
function createSubMenu($str, $method = '')
{
	$arr = explode('|', $str);
	if(!empty($method)){
		foreach($arr as $value) {
			$tmp[] = $value. "." . $method;
		}
		$arr = $tmp;
	}		
	return (array_search(Route::currentRouteName(), $arr) !== false)?  "<li class='sub-menu toggled active'>" : "<li class='sub-menu'>";
}
