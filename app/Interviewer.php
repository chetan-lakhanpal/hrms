<?php

namespace hrms;

use Illuminate\Database\Eloquent\Model;

class Interviewer extends Model
{
    protected $fillable = ['interviewer_id','employee_id'];

    protected $touches = ['employee'];

    public function employee()
    {
    	return $this->belongsTo('hrms\Employee');
    }
}

