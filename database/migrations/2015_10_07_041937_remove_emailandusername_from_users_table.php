<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveEmailandusernameFromUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('name');
            $table->integer('employee_id')->unsigned()->after('id');
            $table->foreign('employee_id')
                  ->references('id')
                  ->on('employees')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users'))
            Schema::table('users', function (Blueprint $table) {
                $table->dropForeign('users_employee_id_foreign');
                $table->dropColumn('employee_id');
                $table->string('name');
                $table->string('email')->unique();
            });
    }
}
