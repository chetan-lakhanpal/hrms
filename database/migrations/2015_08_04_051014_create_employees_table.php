<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->smallInteger('added_by')->unsigned()->nullable();
            $table->smallInteger('source')->unsigned();
            $table->string('other_source', 100);
            //Interviewer in separate table
            $table->char('status', 1);
            $table->string('affiliation', 4);

            $table->string('reject_reason')->nullable();
            $table->string('reject_expertise')->nullable();
            $table->string('reject_ctc')->nullable();

            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('phone_number', 14);
            $table->string('personal_email')->unique();
            $table->date('date_of_birth');
            $table->string('state', 100);
            $table->string('city', 100);
            $table->string('postal_address');
            $table->date('date_of_ann');
            $table->string('landmark', 100);
            $table->string('emergency_no', 14);
            $table->string('blood_group', 3);
            $table->boolean('alcoholic');
            $table->boolean('veg');
            $table->string('emp_type', 20);
            $table->char('payment_type', 1);
            $table->string('emp_id', 20);
            $table->string('office_email')->unique();
            $table->smallInteger('designation')->unsigned();
            $table->smallInteger('department')->unsigned();
            $table->tinyInteger('document')->unsigned();
            $table->date('date_of_joining');
            $table->date('date_of_releving');
            $table->string('resume');

            $table->string('dlc_name')->nullable();
            $table->string('dlc_number', 14)->nullable();
            $table->string('dlc_location', 100)->nullable();

            $table->string('prev_empname');
            $table->string('prev_empcontact', 20);
            $table->string('prev_empemail');
            $table->string('prev_empaddress');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}
