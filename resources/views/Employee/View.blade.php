@extends('layouts.master')

@section('css')
	{!! Html::style('vendors/bootgrid/jquery.bootgrid.min.css') !!}
@stop

@section('meta')
	<meta name="csrf_token" content="{{csrf_token()}}">
	<meta name="route" content="{{Request::url()}}">
@stop

@section('title')
    Employee List
@stop

@section('header')
    Employee List
@stop

@section('content')

<div class="card">
	<div class="card-header">
		<h2>Manage Employees</h2>
	</div>
	 @include('partials.alerts')
	<div class="table-responsive">
		<table id="data-table-selection" class="table table-hover table-vmiddle">
			<thead>
				<tr>
					<th data-column-id="id" data-visible="false" data-identifier="true" data-type="string">Username</th>
					<th data-column-id="name">Name</th>
					<th data-column-id="department">Department</th>
					<th data-column-id="designation">Designation</th>
					<th data-column-id="phone-number">Phone number</th>
					<th data-column-id="email">E-mail</th>
					<th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($employees as $employee)
					<tr>
						<td>{{$employee->slug}}</td>
						<td>{{$employee->affiliation . " " .$employee->first_name . "" . $employee->lastname}}</td>
						<td>{{$employee->department}}</td>
						<td>{{$employee->designation}}</td>
						<td>{{$employee->phone_number}}</td>
						<td>{{$employee->office_email}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@stop

@include('partials.datatable')