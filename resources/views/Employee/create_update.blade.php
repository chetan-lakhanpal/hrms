@include('partials.Empform_resources')
        <div class="row">
            <div class="col-sm-4 m-b-15">
                <p class="f-500 c-black m-b-15">Source</p>
                {!! Form::select('source', ['1' => 'monster.com', '2' => 'naukri.com'], null, ['class' => 'tag-select', 'data-placeholder' => 'Source']) !!}
            </div>
            <div class="col-sm-4 m-b-15">
                <div class="fg-line">
                    <p class="f-500 c-black m-b-10">Other</p>
                    {!! Form::text('other_source', null, ['placeholder' => 'Other', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-4 m-b-15">
                <p class="f-500 c-black m-b-15">Interviewer</p>
                {{-- Binding is being done here i.e interviewrlist name is binded with interviewerlist variable that is in Employee Model--}}
                {!! Form::select('interviewerlist[]', $interviewer, null, ['class' => 'tag-select', 'multiple', 'data-placeholder' => 'Interviewer']) !!}
            </div>
        </div>
        <br>
        <div class="row">
          <div class="col-sm-4 m-b-20">
            <p class="c-black f-500 m-b-20">Status</p>
            <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
               {!! Form::radio('status', 'C') !!}
                 <i class="input-helper"></i>  
               Current Employee
            </label>
         <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
            {!! Form::radio('status', 'E') !!}
            <i class="input-helper"></i>  
            Ex-Employee
        </label>
        <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
            {!! Form::radio('status', 'A') !!}
            <i class="input-helper"></i>  
            Applied
        </label>
        <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
            {!! Form::radio('status', 'R') !!}
            <i class="input-helper"></i>  
            Rejected
        </label>
    </div>

    <div class="col-sm-4 m-b-20">
      <p class="c-black f-500 m-b-20">Affiliation</p>

      <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
         {!! Form::radio('affiliation', 'Mr.') !!}
         <i class="input-helper"></i>  
         Mr.
     </label>

     <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
         {!! Form::radio('affiliation', 'Ms.') !!}
         <i class="input-helper"></i>  
         Ms.
     </label>
     <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
         {!! Form::radio('affiliation', 'Mrs.') !!}
         <i class="input-helper"></i>  
         Mrs.
     </label>
 </div>

 <div class="col-sm-4">

 </div>
 <br>
</div>

</div>
</div>

<div class="card" id="reject" style="display:none">
  <div class="card-header bgm-gray">
      <h2>Rejection details</h2>
  </div>
  <div class="card-body card-padding">
    <div class="row">
        <div class="col-sm-3">                       
            <div class="input-group form-group">
                <span class="input-group-addon"><i class="zmdi zmdi-info-outline"></i></span>
                <div class="fg-line">
                    {!! Form::text('reject_reason', null, ['class' => 'form-control', 'placeholder' => "Reason"]) !!}
                </div>
            </div>
        </div>
        <div class="col-sm-3">                       
            <div class="input-group form-group">
                <span class="input-group-addon"><i class="zmdi zmdi-info-outline"></i></span>
                <div class="fg-line">
                    {!! Form::text('reject_expertise', null, ['class' => 'form-control', 'placeholder' => "Candidate's expertise"]) !!}
                </div>
            </div>
        </div>
        <div class="col-sm-3">                       
            <div class="input-group form-group">
                <span class="input-group-addon"><i class="zmdi zmdi-info-outline"></i></span>
                <div class="fg-line">
                    {!! Form::text('reject_ctc', null, ['class' => 'form-control', 'placeholder' => "CTC Requested"]) !!}
                </div>
            </div>
        </div>
     </div>
   </div>
</div>

<div class="card">
  <div class="card-header">
      <h2>Personal Information</h2>
  </div>
  <div class="card-body card-padding">
    <div class="row">
        <div class="col-sm-3">                       
            <div class="input-group form-group">
                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                <div class="fg-line">
                    {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
                </div>
            </div>

        </div>

        <div class="col-sm-3">                       
            <div class="input-group form-group">
              <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
              <div class="fg-line">
                  {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
              </div>
          </div>

      </div>

      <div class="col-sm-3">                       
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-local-phone"></i></span>
            <div class="fg-line">
              {!! Form::tel('phone_number', null, ['class' => 'form-control', 'placeholder' => 'Contact Number']) !!}
          </div>
      </div>
  </div>

  <div class="col-sm-3">    
    <div class="input-group form-group">
        <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
        <div class="fg-line">
          {!! Form::email('personal_email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
      </div>
  </div>
</div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
            <div class="dtp-container dropdown fg-line">
                {!! Form::text('date_of_birth', null, ['class' => 'form-control date-picker', 'data-toggle' => 'dropdown', 'placeholder' => 'Date of Birth']) !!}
            </div>
        </div>
    </div>

      <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-city"></i></span>
            <div class="fg-line">
                {!! Form::select('state', $states, null, ['class' => 'tag-select', 'data-placeholder' => 'State']) !!}
            </div>
        </div>
    </div>

      <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-city"></i></span>
            <div class="fg-line">
              {!! Form::select('city', $cities, null, ['class' => 'tag-select', 'data-placeholder' => 'City']) !!}
            </div>
        </div>
    </div>

      <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-pin"></i></span>
            <div class="fg-line">
                {!! Form::text('postal_address', null, ['placeholder' => 'Postal Address', 'class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
            <div class="dtp-container dropdown fg-line">
                {!! Form::text('date_of_ann', null, ['class' => 'form-control date-picker', 'data-toggle' => 'dropdown', 'placeholder' => 'Date of Anniversary']) !!}
            </div>
        </div>
    </div>

      <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-city"></i></span>
            <div class="fg-line">
              {!! Form::text('landmark', null, ['class' => 'form-control', 'placeholder' => 'Landmark']) !!}
            </div>
        </div>
    </div>


    <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-local-phone"></i></span>
            <div class="fg-line">
                {!! Form::tel('emergency_no', null, ['placeholder' => 'Emergency Number', 'class' => 'form-control']) !!}
            </div>
        </div>
    </div>

      <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-invert-colors"></i></span>
            <div class="fg-line">
                {!! Form::select('blood_group', $blood_group, null, ['class' => 'tag-select', 'data-placeholder' => 'Blood group']) !!}
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-3 m-b-20">
       <div class="toggle-switch">
        <label for="veg" class="ts-label">Non-Vegetarian</label>
        {!! Form::checkbox('veg', 1, null, ['hidden' => 'hidden', 'id' => 'veg']) !!}
        <label for="veg" class="ts-helper"></label>
        </div>
    </div>

     <div class="col-sm-3 m-b-20">
       <div class="toggle-switch">
        <label for="alcohol" class="ts-label">Alcoholic</label>
         {!! Form::checkbox('alcoholic', 1, null, ['hidden' => 'hidden', 'id' => 'alcohol']) !!}
        <label for="alcohol" class="ts-helper"></label>
        </div>
    </div>
</div>

</div>
</div>{{--personal card close --}}

<div class="card">
  <div class="card-header">
      <h2>Office Information</h2>
  </div>
  <div class="card-body card-padding">
    <div class="row">
        <div class="col-sm-3">
            <p class="c-black f-500 m-b-20">Employee type</p>
              <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
                 {!! Form::radio('emp_type', 'Permanent') !!}
                 <i class="input-helper"></i>  
                 Permanent
             </label>

             <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
                 {!! Form::radio('emp_type', 'Temporary') !!}
                 <i class="input-helper"></i>  
                 Temporary
             </label>
             <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
                 {!! Form::radio('emp_type', 'T20') !!}
                 <i class="input-helper"></i>  
                 T20
             </label>
        </div>
        <div class="col-sm-3">
            <p class="c-black f-500 m-b-20">Payment</p>
              <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
                 {!! Form::radio('payment_type', '1') !!}
                 <i class="input-helper"></i>  
                 on roll
             </label>

             <label class="radio radio-inline m-r-10 m-l-0 m-b-20">
                 {!! Form::radio('payment_type', '0') !!}
                 <i class="input-helper"></i>  
                 off roll
             </label>
             
        </div>
        <div class="col-sm-3">
            <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-account-box"></i></span>
            <div class="fg-line">
              {!! Form::text('emp_id', null, ['class' => 'form-control', 'placeholder' => 'Employee ID']) !!}
            </div>
             </div>
        </div>
        <div class="col-sm-3">
            <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
            <div class="fg-line">
              {!! Form::email('office_email', null, ['class' => 'form-control', 'placeholder' => 'Office E-mail']) !!}
            </div>
             </div>
        </div>
  </div>

<div class="row">
    <div class="col-sm-3 m-t-5">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-format-list-bulleted"></i></span>
            <div class="fg-line">
                {!! Form::select('designation', $designation, null, ['class' => 'tag-select', 'data-placeholder' => 'Designation']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-3 m-t-5">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-format-list-bulleted"></i></span>
            <div class="fg-line">
                {!! Form::select('department', $department, null, ['class' => 'tag-select', 'data-placeholder' => 'Department']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-3 m-t-5">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-check"></i></span>
            <div class="fg-line">
                {!! Form::select('document', $document, null, ['class' => 'tag-select', 'data-placeholder' => 'Documents Submitted']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
            <div class="dtp-container dropdown fg-line">
                {!! Form::text('date_of_joining', null, ['class' => 'form-control date-picker', 'data-toggle' => 'dropdown', 'placeholder' => 'Date of Joining']) !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    
    <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
            <div class="dtp-container dropdown fg-line">
                {!! Form::text('date_of_releving', null, ['class' => 'form-control date-picker', 'data-toggle' => 'dropdown', 'placeholder' => 'Date of Releving']) !!}
            </div>
        </div>
    </div>
  
</div>

</div>
</div>{{--end card--}}

<div class="card" id="t20" style="display:none">
    <div class="card-header bgm-gray">
    <h2>T20</h2>
    </div>
    <div class="card-body card-padding">
        <div class="row">
         <div class="col-sm-3">
            <div class="input-group form-group">
                <span class="input-group-addon"><i class="zmdi zmdi-account-box"></i></span>
                <div class="fg-line">
                  {!! Form::text('dlc_name', null, ['class' => 'form-control', 'placeholder' => 'DLC Name']) !!}
              </div>
          </div>
      </div>
      <div class="col-sm-3">
        <div class="input-group form-group">
            <span class="input-group-addon"><i class="zmdi zmdi-account-box"></i></span>
            <div class="fg-line">
              {!! Form::tel('dlc_number', null, ['class' => 'form-control', 'placeholder' => 'DLC Number']) !!}
          </div>
      </div>
  </div>
  <div class="col-sm-3">
    <div class="input-group form-group">
       <span class="input-group-addon"><i class="zmdi zmdi-city"></i></span>
       <div class="fg-line">
          {!! Form::select('dlc_location', $cities, null, ['class' => 'tag-select', 'data-placeholder' => 'DLC Location']) !!}
      </div>
  </div>
</div>
</div>
</div>
</div>


<div class="card">
    <div class="card-header">
    <h2>Previous Employer's Information</h2>
    </div>
    <div class="card-body card-padding">
        <div class="row">
            <div class="col-sm-3">
             <div class="input-group form-group">
                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                <div class="fg-line">
                  {!! Form::text('prev_empname', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
              </div>
          </div>
      </div> 
      <div class="col-sm-3">
       <div class="input-group form-group">
        <span class="input-group-addon"><i class="zmdi zmdi-local-phone"></i></span>
        <div class="fg-line">
          {!! Form::tel('prev_empcontact', null, ['class' => 'form-control', 'placeholder' => 'Contact number']) !!}
      </div>
  </div>
</div>
<div class="col-sm-3">
   <div class="input-group form-group">
    <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
    <div class="fg-line">
      {!! Form::email('prev_empemail', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
  </div>
</div>
</div>
<div class="col-sm-3">
    <div class="input-group form-group">
        <span class="input-group-addon"><i class="zmdi zmdi-pin"></i></span>
        <div class="fg-line">
          {!! Form::text('prev_empaddress', null, ['class' => 'form-control', 'placeholder' => 'Address']) !!}
      </div>
  </div>
</div>
</div>

<div class="row">
<div class="col-sm-3">
    <div class="input-group form-group">
     <button class="btn btn-info m-r-10" type="Submit">Submit</button>
     <button class="btn btn-default" type="reset">Reset</button>
    </div>
    </div>
</div>

</div>
</div>