@extends('layouts.master')

@section('title')
	Update Employee
@stop

@section('header')
	Update {{$employee->first_name . " " . $employee->last_name}}
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h2>Basic Information</h2>
    </div>
    @include('partials.alerts')
    <div class="card-body card-padding">
	    {!! Form::model($employee, ['method' => 'put', 'action' => ['EmployeeController@update', $employee->id]]) !!}
			@include('Employee.create_update')
        {!! Form::close() !!}
</div>
</div>
@stop
