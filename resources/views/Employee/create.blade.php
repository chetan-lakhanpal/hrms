@extends('layouts.master')

@section('title')
Add Employee
@stop

@section('header')
Add Employee Information
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h2>Basic Information</h2>
    </div>
    @include('partials.alerts')
    <div class="card-body card-padding">
        {!! Form::open(['route' => 'Employee.store', 'files' => true]) !!}
        	@include('Employee.create_update')
        {!! Form::close() !!}
</div>
</div>
@stop
