@extends('layouts.master')

@section('title')
	Create User
@stop

@section('css')
	{!! Html::style('vendors/bootgrid/jquery.bootgrid.min.css') !!}
@stop

@section('meta')
	<meta name="csrf_token" content="{{csrf_token()}}">
	<meta name="route" content="{{Request::url()}}" >
@stop

@section('header')
	Create User
@stop

@section('content')

<div class="card">
	<div class="card-header ch-alt m-b-20">
		<h2> {{isset($mod)? "Update" : "Add"}} Users</h2>
	</div>
	@include('partials.alerts')
	<div class="card-body card-padding">
		<div class="row">
			@if(isset($mod))
				{!! Form::model($mod, ['method' => 'put', 'route' => ['users.update', $mod->id]]) !!}
			@else
				{!! Form::open(['route' => 'users.store']) !!}
			@endif
			<div class="col-sm-4">
				<div class="input-group form-group">
					<span class="input-group-addon"><i class="zmdi zmdi-info-outline "></i></span>
					<div class="fg-line">
					{!! Form::select('employee_id', $emp_list) !!}
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="input-group form-group">
					<span class="input-group-addon"><i class="zmdi zmdi-info-outline "></i></span>
					<div class="fg-line">
					{!! Form::select('role', $role) !!}
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="toggle-switch">
			        <label for="can_edit" class="ts-label">Can edit</label>
			          {!! Form::checkbox('can_edit', 'Y', null, ['hidden' => 'hidden', 'id' => 'can_edit']) !!}
			        <label for="can_edit" class="ts-helper"></label>
		        </div>
			</div>
			<div class="col-sm-4">
				<div class="toggle-switch">
			        <label for="status" class="ts-label">Status</label>
			         {!! Form::checkbox('status', 'Y', null, ['hidden' => 'hidden', 'id' => 'status']) !!}
			        <label for="status" class="ts-helper"></label>
		        </div>
			</div>
			<div class="col-sm-4">
				<div class="input-group form-group">
					<span class="input-group-addon"><i class="zmdi zmdi-info-outline "></i></span>
					<div class="fg-line">
					{!! Form::password('password', null) !!}
					</div>
				</div>
			</div>


			<div class="col-sm-4">
				<div class="input-group form-group">
					<button class="btn btn-info m-r-10" type="Submit">Submit</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@if(!isset($mod))
<div class="card">
	<div class="card-header">
		<h2>Manage Users</h2>
	</div>
	 @include('partials.alerts')
	<div class="table-responsive">
		<table id="data-table-selection" class="table table-hover table-vmiddle">
			<thead>
				<tr>
					<th data-column-id="id" data-visible="false" data-identifier="true" data-type="string">id</th>
					<th data-column-id="name">Name</th>
					<th data-column-id="role">Role</th>
					<th data-column-id="status">Status</th>
					<th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($users as $user)
					<tr>{{ $user->id }}</tr>
					<tr>{{ $user->role }}</tr>
					<tr>{{ $user->status }}</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@include('partials.datatable')
@endif
@stop