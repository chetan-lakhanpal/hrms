@section('css')
    {!! Html::style('vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css') !!}
    {!! Html::style('vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css') !!}
    {!! Html::style('vendors/bower_components/summernote/dist/summernote.css') !!}
    {!! Html::style('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('vendors/farbtastic/farbtastic.css') !!}
    {!! Html::style('vendors/chosen_v1.4.2/chosen.min.css') !!}
@stop
@section('js')
    {!! Html::script('vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js') !!}
    {!! Html::script('vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js') !!}
    {!! Html::script('vendors/bower_components/summernote/dist/summernote.min.js') !!}
    {!! Html::script('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
    
    <!-- Placeholder for IE9 -->
    <!--[if IE 9 ]>
        {!! Html::script('vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js') !!}
    <![endif]-->
    
    {!! Html::script('vendors/chosen_v1.4.2/chosen.jquery.min.js') !!}
    {!! Html::script('vendors/fileinput/fileinput.min.js') !!}
    {!! Html::script('vendors/input-mask/input-mask.min.js') !!}
    {!! Html::script('vendors/farbtastic/farbtastic.min.js') !!}
@stop
@section('custom_js')
<script type="text/javascript">
     $('.alert-success').delay(3000).slideUp(function(){$(this).remove();});
    var $t20 = $('#t20'),
    $reject = $('#reject'),
    $emp_type = $('input[name=emp_type]:radio'),
    $status = $('input[name=status]:radio');
    $status.change(function(e){($(this).val() == 'R')?  $reject.slideDown()   :   $reject.slideUp();});
    $emp_type.change(function(e){($(this).val() == 'T20')?  $t20.slideDown()   :   $t20.slideUp();});
</script>
@stop