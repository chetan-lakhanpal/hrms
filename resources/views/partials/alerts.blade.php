 @if(count($errors))
        <div class="card-body">
          <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
          </div>
          </div>
        @endif

        @if (session('status'))
        <div class="card-body">
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
        </div>
        @endif