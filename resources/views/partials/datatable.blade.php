@section('js')
{!! Html::script('vendors/bootgrid/jquery.bootgrid.min.js') !!}
        <!-- Data Table -->
        <script type="text/javascript">
            $(document).ready(function(){

            var location = window.location.href;
      		 $('.alert-success').delay(3000).slideUp(function(){$(this).remove();});
                var grid = $("#data-table-selection").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'

                    },
                    selection: true, 
                    multiSelect: true,
                    // rowSelect: true,
                    keepSelection: true,
                    formatters: {
                        "commands": function(column, row) {
                            return "<button type=\"button\" class=\"btn btn-icon command-edit\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button> " +
                                "<button type=\"button\" class=\"btn btn-icon command-delete\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";
                        }
                    }
                });

                grid.on("loaded.rs.jquery.bootgrid", function()
				{
				    $(this).find(".command-edit").on("click", function(e)
				    {
				      window.location.href = ($('meta[name=route]').attr('content')) + "/" + $(this).data("row-id") + "/edit";
				    }).end().find(".command-delete").on("click", function(e)
				    {
				        var rows = Array();
				        rows[0] = $(this).data("row-id");
				        swal(
					        {   
			                    title: "Are you sure you want to delete this record?",   
			                    text: "You won't be able to recover it later",   
			                    type: "warning",   
			                    showCancelButton: true,   
			                    confirmButtonColor: "#DD6B55",   
			                    confirmButtonText: "Yes, delete it!",   
			                    closeOnConfirm: false,
			                    showLoaderOnConfirm: true,
			                }, function(){
			                	$.ajax({
			                		headers: {
			                			'X-CSRF-TOKEN': $('meta[name=csrf_token]').attr('content')
			                		},
			                		type: 'DELETE',
			                		url: ($('meta[name=route]').attr('content') + "/" + rows[0]),
			                		success:function(data)
			                		{
			                			if(data.status){
			                				swal("Deleted!", data.name + " has been deleted", "success"); 
						       				grid.bootgrid('remove', rows);
						       			}else{
						       				console.log(data);
						       			}
			                		}
			                	});   
		                	});
				    });
				});
            });
        </script>
@stop