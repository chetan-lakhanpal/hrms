@extends('layouts.master')

@section('css')
	{!! Html::style('vendors/bootgrid/jquery.bootgrid.min.css') !!}
@stop

@section('meta')
<meta name="csrf_token" content="{{csrf_token()}}">
<meta name="route" content="{{Request::url()}}">
@stop

@section('title')
    Manage {{$formname}}s
@stop

@section('header')
    Manage {{$formname}}s
@stop

@section('content')

<div class="card">
	<div class="card-header ch-alt m-b-20">
		<h2> {{isset($mod)? "Update" : "Add"}} {{$formname}}</h2>
	</div>
	@include('partials.alerts')
	<div class="card-body card-padding">
		<div class="row">
		@if(isset($mod))
			{!! Form::model($mod, ['method' => 'put', 'route' => [$route, $mod->id]]) !!}
		@else
			{!! Form::open(['route' => $route]) !!}
		@endif
		<div class="col-sm-4">
			<div class="input-group form-group">
				<span class="input-group-addon"><i class="zmdi zmdi-info-outline "></i></span>
				<div class="fg-line">
					{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => $formname . ' name']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="input-group form-group">
				<button class="btn btn-info m-r-10" type="Submit">Submit</button>
			</div>
			{!! Form::close() !!}
		</div>
				</div>
	</div>
</div>

@if(!isset($mod))
<div class="card">
	<div class="card-header ch-alt m-b-20">
		<h2>Current {{$formname}}s</h2>
	</div>
	<div class="table-responsive">
		<table id="data-table-selection" class="table table-hover table-vmiddle">
			<thead>
				<tr>
					<th data-column-id="id" data-type="numeric" data-identifier="true" data-sortable="false" data-visible="false">ID</th>
					<th data-column-id="name" data-type="string">{{$type}}</th>
					<th data-column-id="commands" data-sortable="false"  data-formatter="commands" data-type="string">Commands</th>
				</tr>
			</thead>
			<tbody>
			
			@foreach ($records as $value)
				<tr>
					<td>{{$value->id}}</td>
					<td>{{$value->name}}</td>
				</tr>
			@endforeach
				
			</tbody>
		</table>
	</div>
</div>
@endif
@stop

@include('partials.datatable')