<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>HRMS</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{asset('css/materialize.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{asset('css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<header>
    <nav class="top-nav">
        <div class="container">
            <div class="nav-wrapper"><a class="page-title">Helpers</a></div>
        </div>
    </nav>
    <div class="container">
        <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only">
            <i class="mdi-navigation-menu"></i>
        </a>
    </div>
    <ul id="slide-out" class="side-nav fixed">
        <li class="logo">HRMS</li>
        <li class="bold active"><a href="#!">First Sidebar Link</a></li>
        <li class="bold"><a href="#!">Second Sidebar Link</a></li>
    </ul>
    <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a
</header>
<main>
    <div class="section" id="index-banner">
        <div class="container">
            <div class="row">
                <div class="col s12 m9">
                    <h1 class="header center-on-small-only">About</h1>
                    <h4 class="light red-text text-lighten-4 center-on-small-only">Learn about the Material Design and our Project Team.</h4>
                </div>
                <div class="col s12 m3">
                    <div class="buysellads-header center-on-small-only">
                        <!-- CarbonAds Zone Code -->
                        <script async="" type="text/javascript" src="//cdn.carbonads.com/carbon.js?zoneid=1673&amp;serve=C6AILKT&amp;placement=materializecss" id="_carbonads_js"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        @yield('content')
        {{--section then row--}}
    </div>
</main>
<footer class="page-footer blue">
    <div class="container">
        @yield('footer')
    </div>
    <div class="footer-copyright">
        <div class="container">
            Made by <a class="white-text text-lighten-3" href="http://materializecss.com">Chetan</a>
        </div>
    </div>
</footer>


<!--  Scripts-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="{{asset('/js/materialize.min.js')}}"></script>
<script src="{{asset('/js/init.js')}}"></script>

</body>
</html>
