            <aside id="sidebar">
                <div class="sidebar-inner c-overflow">
                    <div class="profile-menu">
                        <a href="">
                            <div class="profile-pic">
                                {!! Html::image('img/profile-pics/2.jpg', 'Chetan Lakhanpal') !!}
                            </div>

                            <div class="profile-info">
                                Chetan Lakhanpal
                                <i class="zmdi zmdi-arrow-drop-down"></i>
                            </div>
                        </a>

                        <ul class="main-menu">
                            <li>
                                <a href="profile-about.Html"><i class="zmdi zmdi-account"></i> View Profile</a>
                            </li>
                            <li>
                                <a href=""><i class="zmdi zmdi-settings"></i> Settings</a>
                            </li>
                            <li>
                                <a href=""><i class="zmdi zmdi-time-restore"></i> Logout</a>
                            </li>
                        </ul>
                    </div>

                    <ul class="main-menu">
                        <li><a href="index.Html"><i class="zmdi zmdi-home"></i> Home</a></li>
                        <li><a href="index.Html"><i class="zmdi zmdi-assignment"></i> Manage Leave</a></li>
                        <li><a href="index.Html"><i class="zmdi zmdi-local-cafe"></i> Leave Request</a></li>
                        <li class="sub-menu">
                            <a href=""><i class="zmdi zmdi-book"></i> Documents</a>

                            <ul>
                                <li><a href="widget-templates.Html">Create</a></li>
                                <li><a class="active" href="widgets.Html">Saved</a></li>
                            </ul>
                        </li>
                          {!! createSubMenu('Employee.index|Employee.create') !!}
                            <a href=""><i class="zmdi zmdi-account-box"></i> Employee</a>
                            <ul>
                                {!! create_li('Manage Employee', 'Employee.index') !!}
                                {!! create_li('Add Employee', 'Employee.create') !!}
                                <li><a href="data-tables.Html">Employee Leave</a></li>
                            </ul>
                        </li>
                       {!! createSubMenu('Calendar|departments|designations|sources|checklists', 'index') !!}
                            <a href=""><i class="zmdi zmdi-collection-text"></i> Form info</a>
                            <ul>
                                {!! create_li('Events & Holidays', 'Calendar.index') !!}
                                {!! create_li('Departments', 'departments.index') !!}
                                {!! create_li('Designations', 'designations.index') !!}
                                {!! create_li('Sources', 'sources.index') !!}
                                {!! create_li('Checklists', 'checklists.index') !!}
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a href=""><i class="zmdi zmdi-assignment-account"></i>Admin</a>
                            <ul>
                                {!! create_li('User accounts', 'users.index') !!}
                                <li><a href="animations.Html">News</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </aside>